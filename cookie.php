<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/array.php';
$itemKey = $_GET['buy'];
$items = [];
if(!empty($_COOKIE['items'])){
    $items = json_decode($_COOKIE['items'], true);
}
if(!empty($items[$itemKey])){
    $items[$itemKey]++;
}else{
    $items[$itemKey] = 1;
}
    setcookie('items', json_encode($items), time() + 60 * 60 );
include_once $_SERVER['DOCUMENT_ROOT'].'/html.php';
?>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col" style="text-align: center">
            <h2>Вы добавили новый товар в Корзину</h2>
            <a href="/">Go back to HOMEPAGE</a>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>    
</body>
</html>