<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/array.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/html.php';
$items = [];
$items = json_decode($_COOKIE['items'], true);
?>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-6">
            <table>
                <h3>Список товаров</h3>
                <tr>
                    <th>Название товара</th>
                    <th>Цена</th>
                    <th></th>
                </tr>
                <?php foreach ($goods as $key => $value):?> 
                    <tr>    
                        <th><?=$value['title']?></th>
                        <th><?=$value['price_val']?></th>
                        <th>
                            <form action="/cookie.php" method="get">
                                <button class="btn-warning" name='buy' value='<?=$key++?>'>BUY</button>
                            </form>
                        </th>
                    </tr>
                <?php endforeach;?>
            </table>
        </div>
        <div class="col-6">
            <table>
                    <h3>Корзина</h3>
                        <tr>
                            <th>Наименование товара</th>
                            <th>Количество</th>
                        </tr>
                        <?php if(!empty($_COOKIE['items'])): ?>
                        <?php foreach ($items as $key => $item):?>  
                            <tr>                      
                                <th><?=$goods[$key]['title']?></th>
                                <th><?=$item?></th>
                            </tr>  
                        <?php endforeach;?>      
                        <?php endif;?>
            </table>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>    
</body>
</html>